//
//  PhotoPreviewViewController.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 05/01/22.
//

import UIKit
import Photos

class PhotoPreviewViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ivImagePhoto: UIImageView!

    var imageName: String = ""
    var imgData: String = ""

    // MARK: - Init

    public init() {
        super.init(nibName: "PhotoPreviewViewController", bundle: Bundle(identifier: Constant.bundleId))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        ivImagePhoto.loadImage(url: imgData)
        
        scrollView.maximumZoomScale = 4
        scrollView.minimumZoomScale = 1
        
        scrollView.delegate = self
    }

    @IBAction func onClickCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PhotoPreviewViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return ivImagePhoto
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollView.zoomScale > 1 {
            if let image = ivImagePhoto.image {
                let ratioW = ivImagePhoto.frame.width / image.size.width
                let ratioH = ivImagePhoto.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW : ratioH
                let newWidth = image.size.width * ratio
                let newHeight = image.size.height * ratio
                let conditionLeft = newWidth*scrollView.zoomScale > ivImagePhoto.frame.width
                let left = 0.5 * (conditionLeft ? newWidth - ivImagePhoto.frame.width : (scrollView.frame.width - scrollView.contentSize.width))
                let conditioTop = newHeight * scrollView.zoomScale > ivImagePhoto.frame.height
                let top = 0.5 * (conditioTop ? newHeight - ivImagePhoto.frame.height : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
                
            }
        } else {
            scrollView.contentInset = .zero
        }
    }
}
