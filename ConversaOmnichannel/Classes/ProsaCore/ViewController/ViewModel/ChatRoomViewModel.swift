//
//  ChatRoomViewModel.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 05/01/22.
//

import RxSwift
import RxCocoa
import SwiftyJSON
import UIKit
import QuickLook

class ChatRoomViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    let disposeBag = DisposeBag()
    let dataSource = ChatRoomViewDataSource()
    
    var error = BehaviorRelay<String?>(value: nil)
    var loginRequest = BehaviorRelay<NetworkResponse?>(value: nil)
    var checkTokenRequest = BehaviorRelay<NetworkResponse?>(value: nil)
    var refreshTokenRequest = BehaviorRelay<NetworkResponse?>(value: nil)
    var uploadImageRequest = BehaviorRelay<NetworkResponse?>(value: nil)
    var getBotServiceRequest = BehaviorRelay<NetworkResponse?>(value: nil)
    var deleteChatRequest = BehaviorRelay<NetworkResponse?>(value: nil)

    var loginDataResponse = BehaviorRelay<UserModel?>(value: nil)
    var checkTokenResponse = BehaviorRelay<Bool?>(value: nil)
    var uploadImageResponse = BehaviorRelay<String?>(value: nil)
    var getBotServiceResponse = BehaviorRelay<ProfileBotModel?>(value: nil)
    var deleteChatResponse = BehaviorRelay<String?>(value: nil)

    var messageDataArray: [MessageModel] = []
    var fileArray: [Preview] = []

    required init() {
        super.init()
        registerObserver()
    }
    
    fileprivate func registerObserver() {
        loginRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }
            self?.isLoading.accept(false)

            if response.isSuccess() {
                self?.handleLoginResponse(networkResponse: response)
            }
        }).disposed(by: disposeBag)

        checkTokenRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }
            self?.isLoading.accept(false)

            if response.isSuccess() {
                self?.handleCheckToken(networkResponse: response)
            } else {
                self?.postRefreshToken()
            }
        }).disposed(by: disposeBag)

        refreshTokenRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }
            self?.isLoading.accept(false)

            if response.isSuccess() {
                self?.handleCheckToken(networkResponse: response)
            } else {
                if let response = response.response {
                    let json = JSON(response)
                    self?.error.accept(json["message"].stringValue)
                }
            }
        }).disposed(by: disposeBag)
        
        uploadImageRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }

            if response.isSuccess() {
                self?.handleUploadImage(networkResponse: response)
            } else {
                if let response = response.response {
                    let json = JSON(response)
                    self?.error.accept(json["message"].stringValue)
                }
            }
        }).disposed(by: disposeBag)

        getBotServiceRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }

            if response.isSuccess() {
                self?.handleChatBotResp(networkResponse: response)
            } else {
                if let response = response.response {
                    let json = JSON(response)
                    self?.error.accept(json["message"].stringValue)
                }
            }
        }).disposed(by: disposeBag)

        deleteChatRequest.subscribe(onNext: { [weak self] response in
            guard let response = response else {  return }

            if response.isSuccess() {
                self?.handleDeleteChatResponse(networkResponse: response)
            } else {
                if let response = response.response {
                    let json = JSON(response)
                    self?.error.accept(json["message"].stringValue)
                }
            }
        }).disposed(by: disposeBag)
    }

    fileprivate func handleLoginResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        loginDataResponse.accept(UserModel(json))
    }
    
    fileprivate func handleUploadImage(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        uploadImageResponse.accept(json["url"].stringValue)
    }

    fileprivate func handleCheckToken(networkResponse: NetworkResponse) {
        if let response = networkResponse.response {
            let json = JSON(response)
            if json["access_token"].exists() {
                UserDefaults.standard.set(json["access_token"].stringValue, forKey: Constant.USER_TOKEN)
            }
        }
        checkTokenResponse.accept(true)
    }
    
    fileprivate func handleChatBotResp(networkResponse: NetworkResponse) {
        if let response = networkResponse.response {
            let json = JSON(response)
            getBotServiceResponse.accept(ProfileBotModel(json))
        }
    }
    
    fileprivate func handleDeleteChatResponse(networkResponse: NetworkResponse) {
        if let response = networkResponse.response {
            let json = JSON(response)
            deleteChatResponse.accept(json["message"].stringValue)
        }
    }
    
    func postLoginReq(userId: String, userName: String, password: String, avatarUrl: String) {
        isLoading.accept(true)
        let appID = UserDefaults.standard.string(forKey: Constant.APP_ID) ?? ""
        var params: [String: Any] = [:]
        params["userid"] = userId
        params["name"] = userName
        params["password"] = password
        params["application_id"] = appID
        params["avatar_url"] = avatarUrl

        dataSource.getUserProfile(params: params, networkResponse: loginRequest)
    }

    func postCheckToken(token: String) {
        isLoading.accept(true)

        dataSource.postCheckToken(params: [:], networkResponse: checkTokenRequest)
    }

    func postRefreshToken() {
        isLoading.accept(true)

        dataSource.postRefreshToken(params: [:], networkResponse: refreshTokenRequest)
    }

    func uploadData(data: URL, fileData: Data) {
        dataSource.uploadImage(file: data, fileData: fileData, networkResponse: uploadImageRequest)
    }

    func deleteMessage(serverId: String) {
        dataSource.deleteMessage(serverChatId: serverId, networkResponse: deleteChatRequest)
    }

    func deleteFileArray(urlPrefix: String) {
        for (index, data) in fileArray.enumerated() {
            if data.urlPrefix == urlPrefix {
                fileArray.remove(at: index)
                Dlog("Removed on index \(index)")
            }
        }
    }
    
    func getBotService() {
        dataSource.getDetailBot(networkResponse: getBotServiceRequest)
    }
    
    func cancelUpload() {
        dataSource.cancelUpload()
    }
    
    func getFilename(urlPrefix: String) -> URL? {
        for data in fileArray {
            if data.urlPrefix == urlPrefix {
                return data.url
            }
        }
        return URL(string: "")
    }

    func registerFont() {
        UIFont.register(fileNameString: "Ubuntu-Bold", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-BoldItalic", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-Italic", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-Light", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-LightItalic", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-Medium", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-MediumItalic", type: ".ttf")
        UIFont.register(fileNameString: "Ubuntu-Regular", type: ".ttf")
    }

    func changeStatusMessage(status: String, clientId: Int) {
        if let row = self.messageDataArray.firstIndex(where: {$0.clientChatId == clientId}) {
            let statusNow = messageDataArray[row].status
            if statusNow == "READ" {
                return
            } else if statusNow == "SENDING" && status == "READ" {
                messageDataArray[row].status = status
            } else if statusNow == "RECEIVED" && status == "SENDING" {
                messageDataArray[row].status = status
            } else if statusNow == "PENDING" && status == "RECEIVED" {
                messageDataArray[row].status = status
            } else {
                messageDataArray[row].status = status
            }
        }
    }

    func addServerId(serverId: String, clientId: Int) {
        if let row = self.messageDataArray.firstIndex(where: {$0.clientChatId == clientId}) {
            messageDataArray[row].serverChatId = serverId
        }
    }
    
    func groupingComments(_ data: [MessageModel]) {
        let ready = data.sorted(by: { $0.timestamp?.compare($1.timestamp ?? Date()) == .orderedDescending })
        var retVal = [[MessageModel]]()
        let groupedMessages = Dictionary(grouping: ready) { (element) -> Date in
            return element.timestamp?.reduceToMonthDayYear() ?? Date()
        }
        let sortedKeys = groupedMessages.keys.sorted(by: { $0.compare($1) == .orderedDescending })
        sortedKeys.forEach { (key) in
            let values = groupedMessages[key]
            retVal.append(values ?? [])
        }
        for data in retVal {
            for message in data {
                messageDataArray.append(message)
            }
            messageDataArray.append(ParseMessageDate(["date": data.first?.timestamp ?? Date()]))
        }
    }

    func deletedMessage(_ messageServerIdDeleted: String) {
        for (index, data) in messageDataArray.enumerated() {
            if data.isSender() {
                if let serverChatId = data.serverChatId {
                    if serverChatId == messageServerIdDeleted {
                        messageDataArray.remove(at: index)
                    }
                }
            }
        }
    }
}
