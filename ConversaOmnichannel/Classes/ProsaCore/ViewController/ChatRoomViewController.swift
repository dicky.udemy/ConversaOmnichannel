//
//  ChatRoomViewController.swift
//  ProsaSDK
//
//  Created by Dicky Geraldi on 02/01/22.
//

import Toast
import QuickLook
import UIKit
import Photos

class ChatRoomViewController: BaseViewController<ChatRoomViewModel> {

    // MARK: - Properties

    @IBOutlet weak var ivImageProfile: UIImageView!
    @IBOutlet weak var lblNameAsistannt: UILabel!
    @IBOutlet weak var lblVirtualAsistantSublabel: UILabel!
    @IBOutlet weak var tvTableChat: UITableView!
    @IBOutlet weak var tfMessageTextfield: UITextField!
    @IBOutlet weak var vwReplyview: UIView!
    @IBOutlet weak var lblReplySenderName: UILabel!
    @IBOutlet weak var lblReplyMessage: UILabel!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var vwProgressView: ProgressView!
    @IBOutlet weak var btnSendChat: UIButton!
    @IBOutlet weak var vwHiddenContent: UIView!
    @IBOutlet weak var vwStatusSocket: UIView!
    @IBOutlet weak var ivIconStatus: UIImageView!
    @IBOutlet weak var lblStatusConnection: UILabel!
    @IBOutlet weak var ivPreviewImageReply: UIImageView!
    @IBOutlet weak var ivIconPreview: UIImageView!
    
    private var imagePicker: ImagePicker!
    private var documentPicker: DocumentPicker!
    private var section: Int = 0
    private var dateInsert: Date = Date()
    private var count = 3
    private var chatIdDeleted: String = ""
    private let previewVC = QLPreviewController()
    private lazy var previewItem = URL(string: "")
    private var refreshControl: UIRefreshControl = UIRefreshControl()
    private let previewController = QLPreviewController()
    let socketManager = SocketIoManager.sharedInstance
    var isReply: Bool = false
    var arrayCoba: [Int] = []
    var avatarUrl: String = ""
    var password: String = ""

    // MARK: - Init

    init() {
        super.init(nibName: "ChatRoomView", bundle: Bundle(identifier: Constant.bundleId))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel?.registerFont()
        previewVC.dataSource = self
        tfMessageTextfield.delegate = self
        tfMessageTextfield.setLeftPaddingPoints(15)
        tfMessageTextfield.setRightPaddingPoints(10)

        let (status, userId, userName) = checkUserData()
        if status {
            viewModel?.getBotService()
            let token = UserDefaults.standard.string(forKey: Constant.USER_TOKEN) ?? ""
            if token.isEmpty {
                viewModel?.postLoginReq(userId: userId, userName: userName, password: password, avatarUrl: avatarUrl)
            } else {
                viewModel?.postCheckToken(token: token)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.isHidden = true
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        tfMessageTextfield.resignFirstResponder()
    }

    override func registerObserver() {
        viewModel?.error.subscribe(onNext: { [weak self] (message) in
            if let self = self {
                DispatchQueue.main.async {
                    self.view.makeToast(message)
                    self.setupView()
                }
            }
        }).disposed(by: disposeBag)

        viewModel?.isLoading.subscribe(onNext: { [weak self] (isLoading) in
            if let self = self {
                DispatchQueue.main.async {
                    self.vwHiddenContent.isHidden = !isLoading
                    self.vwProgressView.isAnimating = isLoading
                    self.vwProgressView.isHidden = !isLoading
                }
            }
        }).disposed(by: disposeBag)

        viewModel?.loginDataResponse.subscribe(onNext: { [weak self] userData in
            if let self = self, let userDetail = userData {
                DispatchQueue.main.async {
                    UserDefaults.standard.set(userDetail.accessToken ?? "", forKey: Constant.USER_TOKEN)
                    UserDefaults.standard.set(userDetail.refreshToken ?? "", forKey: Constant.USER_REFRESH_TOKEN)
                    self.setupView()
                    self.socketManager.setupSocket()
                    self.socketManager.delegate = self
                }
            }
        }).disposed(by: disposeBag)

        viewModel?.checkTokenResponse.subscribe(onNext: { [weak self] isActive in
            if let self = self, let isActive = isActive {
                DispatchQueue.main.async {
                    if isActive {
                        self.setupView()
                        self.socketManager.setupSocket()
                        self.socketManager.delegate = self
                    }
                }
            }
        }).disposed(by: disposeBag)

        viewModel?.uploadImageResponse.subscribe(onNext: { [weak self] url in
            if let self = self, let url = url {
                var replyTo: String = ""
                var clientChatId: Int = 0
                var prefixName: String = ""
                if self.isReply {
                    replyTo = self.lblReplyMessage.text ?? ""
                }
                if let data = self.viewModel?.fileArray.last {
                    data.urlPrefix = (url as NSString).lastPathComponent
                    prefixName = data.urlPrefix
                }
                if let fileSend = self.viewModel?.messageDataArray.first {
                    fileSend.documentSource = url
                    clientChatId = fileSend.clientChatId ?? 0
                }
                self.socketManager.send(message: "", replyTo: replyTo, source: "USER", documentUrl: url, clientChatId: clientChatId, docName: prefixName)
            }
        }).disposed(by: disposeBag)

        viewModel?.getBotServiceResponse.subscribe(onNext: { [weak self] data in
            if let self = self, let profileBot = data {
                self.setBotService(data: profileBot)
            }
        }).disposed(by: disposeBag)
        
        viewModel?.deleteChatResponse.subscribe(onNext: { [weak self] data in
            if let self = self, let message = data {
                if message == "OK" {
                    DispatchQueue.main.async {
                        self.viewModel?.deletedMessage(self.chatIdDeleted)
                        self.view.makeToast("Delete Message Success")
                        self.tvTableChat.reloadData()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }

    // MARK: - Actions

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendMessageClick(_ sender: Any) {
        if let message = tfMessageTextfield.text {
            var replyTo: String = ""
            if isReply {
                replyTo = lblReplyMessage.text ?? ""
            }
            socketManager.send(message: message, replyTo: replyTo, source: "USER", documentUrl: "", clientChatId: Int.random(in: 0..<1000000))
        }
        tfMessageTextfield.text = ""
        btnSendChat.isEnabled = false
        btnSendChat.isUserInteractionEnabled = true
        btnSendChat.setImage(UIImage(named: "icon-send-inactive", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil), for: .normal)
        
        if isReply {
            vwReplyview.isHidden = true
        }
    }

    @IBAction func onClickImage(_ sender: UIButton) {
        showAlertClickPhoto(sender)
    }

    @IBAction func onClickAttachment(_ sender: Any) {
        documentPicker.present()
    }

    @IBAction func onClickCloseReply(_ sender: Any) {
        self.isReply = false
        vwReplyview.isHidden = true
    }

    @objc func updateCounter() {
        if count > 0 {
             count -= 1
        }

        if count == 0 {
            setView(view: vwStatusSocket, hidden: true)
        }
    }

    @objc func refresh() {
        socketManager.setupSocket()
    }
}

// MARK: - Textfield delegate

extension ChatRoomViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
            btnSendChat.isEnabled = !updatedString.isEmpty
            btnSendChat.isUserInteractionEnabled = !updatedString.isEmpty

            if updatedString.isEmpty {
                btnSendChat.setImage(UIImage(named: "icon-send-inactive", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil), for: .normal)
            } else {
                btnSendChat.setImage(UIImage(named: "icon-send-active", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil), for: .normal)
            }
        }
        
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

// MARK: - Helper View

extension ChatRoomViewController {
    private func checkUserData() -> (Bool, String, String) {
        let userId = UserDefaults.standard.string(forKey: Constant.USER_ID) ?? ""
        let userName = UserDefaults.standard.string(forKey: Constant.USER_NAME) ?? ""

        if userId.isEmpty || userName.isEmpty {
            showAlert()
            return (false, "", "")
        }

        return (true, userId, userName)
    }

    private func setupTableView() {
        tvTableChat.delegate = self
        tvTableChat.dataSource = self
        tvTableChat.keyboardDismissMode = .onDrag
        tvTableChat.separatorStyle = .none
        tvTableChat.estimatedRowHeight = 100
        tvTableChat.rowHeight = UITableView.automaticDimension
        let rotate = CGAffineTransform(rotationAngle: .pi)
        tvTableChat.transform = rotate
        tvTableChat.scrollsToTop = false
        tvTableChat.allowsSelection = false
        tvTableChat.showsVerticalScrollIndicator = false

        tvTableChat.register(UINib(nibName: "BubbleChatViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleChatViewCell")
        tvTableChat.register(UINib(nibName: "BubbleHeaderViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleHeaderViewCell")
        tvTableChat.register(UINib(nibName: "BubbleChatSenderViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleChatSenderViewCell")
        tvTableChat.register(UINib(nibName: "BubbleChatImageViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleChatImageViewCell")
        tvTableChat.register(UINib(nibName: "BubbleChatImageSenderViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleChatImageSenderViewCell")
        tvTableChat.register(UINib(nibName: "BubbleDocsSenderViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleDocsSenderViewCell")
        tvTableChat.register(UINib(nibName: "BubbleDocsViewCell", bundle: Bundle(identifier: Constant.bundleId)), forCellReuseIdentifier: "BubbleDocsViewCell")
    }

    private func setupView() {
        setupTableView()
        setupKeyboard()
        tfMessageTextfield.clipsToBounds = true
        tfMessageTextfield.layer.cornerRadius = 10
        tfMessageTextfield.layer.borderWidth = 1
        tfMessageTextfield.layer.borderColor = UIColor.white.cgColor
        tfMessageTextfield.backgroundColor = .white

        vwReplyview.isHidden = true
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        documentPicker = DocumentPicker(presentationController: self, delegate: self)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
//        tvTableChat.addSubview(refreshControl)
        ivPreviewImageReply.layer.cornerRadius = 10
    }

    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotif), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotif), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    private func showAlert() {
        let alert = UIAlertController(title: "Error", message: "Please Setup User Data", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    private func setBotService(data: ProfileBotModel) {
        ivImageProfile.loadImage(url: data.propicUrl)
        lblNameAsistannt.text = data.chatbotName
        lblVirtualAsistantSublabel.text = data.description

        ivImageProfile.makeRounded()
    }

    private func setView(view: UIView, hidden: Bool) {
        if !hidden {
            UIView.transition(with: view, duration: 1, options: .curveEaseIn, animations: {
                view.isHidden = hidden
            })
        } else {
            UIView.transition(with: view, duration: 1, options: .curveEaseOut, animations: {
                view.isHidden = hidden
            })
        }
    }
}

// MARK: - UITableView

extension ChatRoomViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.messageDataArray.count ?? 0
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let message = viewModel?.messageDataArray[indexPath.row] {
            if message.clientChatId == nil {
                if message.text != nil {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleHeaderViewCell") as? BubbleHeaderViewCell {
                        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                        cell.selectionStyle = .none
                        cell.setView(text: message.text ?? "")
                        return cell
                    }
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEEE, MMM d yyyy"
                    let dateString = dateFormatter.string(from: message.timestamp ?? Date())

                    if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleHeaderViewCell") as? BubbleHeaderViewCell {
                        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                        cell.selectionStyle = .none
                        cell.setView(text: dateString)
                        return cell
                    }
                }
            } else {
                if (message.documentSource != "" || !message.getHtmltag().isEmpty) {
                    let url = message.documentSource ?? ""
                    let messageUrl = message.getHtmltag()
                    
                    if url.isEmpty {
                        message.documentSource = messageUrl
                    }
                    if url.isImage() || messageUrl.isImage() {
                        if message.isSender() {
                            if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleChatImageViewCell") as? BubbleChatImageViewCell {
                                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                                cell.selectionStyle = .none
                                cell.data = message
                                cell.setView()
                                cell.delegate = self
                                cell.onClick = { data in
                                    let vc = PhotoPreviewViewController()
                                    vc.imageName = ((message.documentSource ?? "") as NSString).lastPathComponent
                                    vc.imgData = data.documentSource ?? messageUrl
                                    vc.modalPresentationStyle = .overCurrentContext
                                    self.present(vc, animated: true, completion: nil)
                                }
                                cell.onClickChatOptions = { data in
                                    self.showAlertClick(data: data)
                                }
                                return cell
                            }
                        } else {
                            if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleChatImageSenderViewCell") as? BubbleChatImageSenderViewCell {
                                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                                cell.selectionStyle = .none
                                cell.data = message
                                cell.setView()
                                cell.onClick = { data in
                                    let vc = PhotoPreviewViewController()
                                    vc.imageName = ((message.documentSource ?? "") as NSString).lastPathComponent
                                    vc.imgData = data.documentSource ?? messageUrl
                                    vc.modalPresentationStyle = .overCurrentContext
                                    self.present(vc, animated: true, completion: nil)
                                }
                                cell.onClickChatOptions = { data in
                                    self.showAlertClick(data: data)
                                }
                                return cell
                            }
                        }
                    } else {
                        if message.isSender() {
                            if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleDocsSenderViewCell") as? BubbleDocsSenderViewCell {
                                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                                cell.selectionStyle = .none
                                if let url = message.documentSource {
                                    guard let url = URL(string: url) else {
                                        return UITableViewCell()
                                    }
                                    cell.nameOfFile = url.lastPathComponent
                                    cell.onClickOpenFile = {
                                        self.quickLook(url: url)
                                    }
                                    cell.delegate = self
                                }
                                cell.chatModel = message
                                cell.setupCell()
                                cell.onClickChatOptions = { data in
                                    self.showAlertClick(data: data)
                                }
                                return cell
                            }
                        } else {
                            if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleDocsViewCell") as? BubbleDocsViewCell {
                                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                                cell.selectionStyle = .none
                                if let url = message.documentSource {
                                    guard let url = URL(string: url) else {
                                        return UITableViewCell()
                                    }
                                    cell.nameOfFile = url.lastPathComponent
                                    cell.onClickOpenFile = {
                                        self.quickLook(url: url)
                                    }
                                }
                                cell.chatModel = message
                                cell.setupCell()
                                cell.onClickChatOptions = { data in
                                    self.showAlertClick(data: data)
                                }
                                return cell
                            }
                        }
                    }
                } else {
                    if message.isSender() {
                        if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleChatSenderViewCell") as? BubbleChatSenderViewCell {
                            cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                            cell.selectionStyle = .none
                            cell.chatModel = message
                            cell.setupCell()
                            cell.onClickChatOptions = { data in
                                self.showAlertClick(data: data)
                            }
                            return cell
                        }
                    } else {
                        if let cell = tableView.dequeueReusableCell(withIdentifier: "BubbleChatViewCell") as? BubbleChatViewCell {
                            cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                            cell.selectionStyle = .none
                            cell.chatModel = message
                            cell.setupCell()
                            cell.onClickChatOptions = { data in
                                self.showAlertClick(data: data)
                            }
                            return cell
                        }
                    }
                }
            }
        }

        return UITableViewCell()
    }
}

// MARK: - Objc Action

extension ChatRoomViewController {
    @objc func handleKeyboardNotif(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let isKeyboardShow = (notification.name == UIResponder.keyboardWillShowNotification)
                constraintBottom.constant = isKeyboardShow ? -keyboardRectangle.height : 0

                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (completed) in
                    if (self.viewModel?.messageDataArray.count ?? 0) > 0 {
                        self.tvTableChat.scrollToRow(
                            at: IndexPath(row: 0,
                                          section: 0), at: .bottom, animated: true)
                    }
                })
            }
        }
    }
}

// MARK: - UIAlert

extension ChatRoomViewController {
    func showAlertClick(data: MessageModel) {
        let alert = UIAlertController(title: "Chat Options", message: "You can choose one options", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Reply", style: .default, handler: { (_) in
            self.isReply = true
            var username: String = self.lblNameAsistannt.text ?? ""
            if data.isSender() {
                username = UserDefaults.standard.string(forKey: Constant.USER_NAME) ?? ""
            }
            self.vwReplyview.isHidden = false
            self.lblReplyMessage.attributedText = data.text?.htmlAttributedString()
            self.lblReplyMessage.textColor = UIColor(hex: "FFFCFC")
            self.lblReplySenderName.text = username
            if let source = data.documentSource {
                self.ivPreviewImageReply.loadImage(url: source)
                self.ivPreviewImageReply.isHidden = (source).isEmpty
                self.ivIconPreview.isHidden = (source).isEmpty
                if source.isImage() {
                    self.ivIconPreview.image = UIImage(named: "icon-image")
                } else {
                    self.ivIconPreview.image = UIImage(named: "icon-docs")
                }
            }
            self.tfMessageTextfield.becomeFirstResponder()
        }))

        if (data.documentSource ?? "").isEmpty || !data.isHtmlDoc() {
            alert.addAction(UIAlertAction(title: "Copy Text", style: .default, handler: { (_) in
                if let text = data.text {
                    let str = text.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                    UIPasteboard.general.string = str
                }
            }))
        }

        if data.isSender() {
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
                self.chatIdDeleted = data.serverChatId ?? ""
                self.viewModel?.deleteMessage(serverId: data.serverChatId ?? "")

                if let url = data.documentSource {
                    self.viewModel?.deleteFileArray(urlPrefix: (url as NSString).lastPathComponent)
                }
            }))
        }

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }

    func showAlertClickPhoto(_ sender: UIButton) {
        imagePicker.present(from: sender)
    }
}

// MARK: - ImagePicker

extension ChatRoomViewController: BubbleDocsCancelDelegate {
    func cancelUpload() {
        viewModel?.cancelUpload()
        viewModel?.messageDataArray.removeFirst()
        tvTableChat.reloadData()
    }
}

// MARK: - ImagePicker

extension ChatRoomViewController: ImagePickerDelegate {
    func didSelect(url: URL, image: UIImage?) {
        messageSend(message: ParseMessageModelFromParams(
            ["room": socketManager.room,
             "document_source": url.relativePath,
             "client_chat_id": Int.random(in: 0..<1000000),
             "source": "USER"], status: "PENDING"))
        viewModel?.fileArray.append(Preview(url: url, urlPrefix: url.lastPathComponent))
        tvTableChat.reloadData()
        viewModel?.uploadData(data: url, fileData: image?.jpegData(compressionQuality: 0.4) ?? Data())
    }
}

// MARK: - Document Picker

extension ChatRoomViewController: DocumetPickerDelegate {
    func didSelect(document: URL) {
        do {
            messageSend(message: ParseMessageModelFromParams(
                ["room": socketManager.room,
                 "document_source": document.lastPathComponent,
                 "client_chat_id": Int.random(in: 0..<1000000),
                 "source": "USER"], status: "PENDING"))
            viewModel?.fileArray.append(Preview(url: document, urlPrefix: document.lastPathComponent))
            self.tvTableChat.reloadData()
            let data = try Data(contentsOf: document)
            viewModel?.uploadData(data: document, fileData: data)
        } catch {
            print("document loading error")
        }
    }
}

// MARK: - Quicklook

extension ChatRoomViewController: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        if let url = previewItem {
            return url as QLPreviewItem
        }
        return previewItem! as QLPreviewItem
    }
    
    func quickLook(url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                self.presentAlertController(with: error?.localizedDescription ?? "Failed to download the pdf!!!")
                return
            }

            guard let httpURLResponse = response as? HTTPURLResponse else { return }
            
            do {
                let suggestedFilename = httpURLResponse.suggestedFilename ?? "\(UUID().uuidString)_\(url.lastPathComponent)"
                var previewURL = FileManager.default.temporaryDirectory.appendingPathComponent(suggestedFilename)
                try data.write(to: previewURL, options: .atomic)
                self.previewItem = previewURL
                previewURL.hasHiddenExtension = true
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.previewController.dataSource = self
                    self.previewController.currentPreviewItemIndex = 0
                    self.present(self.previewController, animated: true)
                 }
            } catch {
                print(error)
                return
            }
        }.resume()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func presentAlertController(with message: String) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            alert.addAction(.init(title: "OK", style: .default))
            self.present(alert, animated: true)
        }
    }
}

// MARK: - Socket Delegation

extension ChatRoomViewController: SocketIoDelegate {
    func messageSend(message: MessageModel) {
        viewModel?.messageDataArray.insert(message, at: 0)
        tvTableChat.reloadData()
    }
    
    func messageReadByServer(clientId: Int) {
        viewModel?.changeStatusMessage(status: "READ", clientId: clientId)
        tvTableChat.reloadData()
    }

    func messageServerIdReceived(serverId: String, clientId: Int) {
        viewModel?.changeStatusMessage(status: "SENDING", clientId: clientId)
        viewModel?.addServerId(serverId: serverId, clientId: clientId)
        tvTableChat.reloadData()
    }
    
    func messageReceivedServer(clientId: Int) {
        viewModel?.changeStatusMessage(status: "RECEIVED", clientId: clientId)
        tvTableChat.reloadData()
        if (viewModel?.messageDataArray.count ?? 0) > 0 {
            tvTableChat.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    
    func messageReceived(message: MessageModel) {
        if !(message.source == "USER") {
            viewModel?.messageDataArray.insert(message, at: 0)
            socketManager.read(serverChatId: message.serverChatId ?? "")
        }
        viewModel?.changeStatusMessage(status: "SENDING", clientId: message.clientChatId ?? 0)
        tvTableChat.reloadData()
    }
    
    func messageHistoryReceived(message: [MessageModel]) {
        let data = viewModel?.messageDataArray ?? []
        if data.isEmpty {
            viewModel?.groupingComments(message)
            tvTableChat.reloadData()
        }
    }

    func messageAgentJoined(message: MessageModel) {
        viewModel?.messageDataArray.insert(message, at: 0)
        tvTableChat.reloadData()
    }

    func statusSocket(isConneected: Bool, isReconnect: Bool) {
        ivIconStatus.isHidden = !isReconnect
        if isReconnect {
            vwStatusSocket.backgroundColor = UIColor(hex: "C4C4C4")
            lblStatusConnection.text = "No Connection, Reconnecting....."
        } else {
            if isConneected {
                vwStatusSocket.backgroundColor = UIColor(hex: "00E540")
                lblStatusConnection.text = "Connected"
            } else {
                vwStatusSocket.backgroundColor = UIColor(hex: "FF3D3D")
                lblStatusConnection.text = "Connection Error, Reload to connect"
            }
        }
        setView(view: vwStatusSocket, hidden: false)
        if count == 0 {
            count = 10
        }
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
}
