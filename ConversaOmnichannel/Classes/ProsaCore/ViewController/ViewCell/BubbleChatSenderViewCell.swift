//
//  BubbleChatSenderViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 03/01/22.
//

import UIKit

class BubbleChatSenderViewCell: UITableViewCell {

    @IBOutlet weak var lblTimeSender: UILabel!
    @IBOutlet weak var lblTextMessage: UILabel!
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var btnStatusMessage: UIImageView!
    
    var chatModel: MessageModel?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()

        vwContent.clipsToBounds = true
        vwContent.layer.cornerRadius = 5
    }

    func setupCell() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"

        if let data = chatModel {
            lblTextMessage.attributedText = data.text?.htmlAttributedString()
            lblTextMessage.textColor = UIColor(hex: "FFFCFC")
            lblTimeSender.text = dateFormatter.string(from: data.timestamp ?? Date())

            if data.status == "PENDING" {
                btnStatusMessage.image = UIImage(named: "icon-pending", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "RECEIVED" {
                btnStatusMessage.image = UIImage(named: "icon-message-send", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "SENDING" {
                btnStatusMessage.image = UIImage(named: "icon-message-delivered", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "READ" {
                btnStatusMessage.image = UIImage(named: "icon-message-read", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            }
        }
    }

    @IBAction func onClickChatOptions(_ sender: Any) {
        onClickChatOptions?(chatModel!)
    }
}
