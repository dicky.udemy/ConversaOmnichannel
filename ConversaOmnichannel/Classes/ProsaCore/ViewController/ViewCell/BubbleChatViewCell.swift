//
//  BubbleChatViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 03/01/22.
//

import UIKit

class BubbleChatViewCell: UITableViewCell {

    @IBOutlet weak var lblTextMessage: UILabel!
    @IBOutlet weak var lblTimeSend: UILabel!
    @IBOutlet weak var vwContent: UIView!
    
    var chatModel: MessageModel?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()

        vwContent.clipsToBounds = true
        vwContent.layer.cornerRadius = 5
    }

    func setupCell() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"

        lblTextMessage.attributedText = chatModel?.text?.htmlAttributedString()
        lblTimeSend.text = dateFormatter.string(from: chatModel?.timestamp ?? Date())
    }

    @IBAction func onClickChatOption(_ sender: Any) {
        onClickChatOptions?(chatModel!)
    }
}
