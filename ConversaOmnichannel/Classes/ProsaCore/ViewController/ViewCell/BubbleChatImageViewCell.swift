//
//  BubbleChatImageViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 11/01/22.
//

import UIKit
import UICircularProgressRing

class BubbleChatImageViewCell: UITableViewCell {

    @IBOutlet weak var vwContentChat: UIView!
    @IBOutlet weak var btnCancelUpload: UIButton!
    @IBOutlet weak var vwCircular: UICircularProgressRing!
    @IBOutlet weak var ivImageChat: UIImageView!
    @IBOutlet weak var lblSendDate: UILabel!
    @IBOutlet weak var btnShowImage: UIButton!
    @IBOutlet weak var btnStatusMessage: UIImageView!
    
    private let notificationCenter = NotificationCenter.default
    var onClick: ((_ data: MessageModel) -> Void)?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?
    var data: MessageModel?
    weak var delegate: BubbleDocsCancelDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        vwContentChat.clipsToBounds = true
        vwContentChat.layer.cornerRadius = 5
    }

    func setView() {
        if let data = data {
            vwCircular.isHidden = true
            do {
                let data = try Data(contentsOf: URL(string: data.documentSource ?? "")!)
                ivImageChat.image = UIImage(data: data)
            } catch {
                print("document loading error")
            }
            ivImageChat.layer.cornerRadius = 5
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            lblSendDate.text = dateFormatter.string(from: data.timestamp ?? Date())

            if data.status == "PENDING" {
                btnShowImage.isHidden = true
                initNotifCenter()
                vwCircular.isHidden = false
                btnStatusMessage.image = UIImage(named: "icon-pending", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "RECEIVED" {
                btnShowImage.isHidden = false
                btnStatusMessage.image = UIImage(named: "icon-message-send", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "SENDING" {
                btnShowImage.isHidden = false
                btnStatusMessage.image = UIImage(named: "icon-message-delivered", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "READ" {
                btnShowImage.isHidden = false
                btnStatusMessage.image = UIImage(named: "icon-message-read", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            }
        }
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    @IBAction func onclickCancel(_ sender: Any) {
        delegate?.cancelUpload()
    }
    
    func initNotifCenter() {
        notificationCenter.addObserver(self, selector: #selector(uploadStart), name: Notification.Name("upload-start"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(uploadProgress(notification:)), name: Notification.Name("upload-progress"), object: nil)
    }
    
    @IBAction func onClick(_ sender: Any) {
        print("CLIKED:")
        onClick?(data ?? MessageModel())
    }
    
    @IBAction func onClickChatOptions(_ sender: Any) {
        onClickChatOptions?(data!)
    }
    
    
    @objc func uploadStart() {
        vwCircular.value = 10
    }
    
    @objc func uploadProgress(notification: Notification) {
        if let data = notification.userInfo {
            if let progress = data["progress"] as? Double {
                vwCircular.value = progress
                if progress == 1 {
                    ivImageChat.image = UIImage(named: "icon-message-send", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                }
            }
        }
    }
}
