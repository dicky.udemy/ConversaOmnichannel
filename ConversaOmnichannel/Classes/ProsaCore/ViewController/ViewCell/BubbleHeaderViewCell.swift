//
//  BubbleHeaderViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 03/01/22.
//

import UIKit

class BubbleHeaderViewCell: UITableViewCell {

    @IBOutlet weak var vwContentChat: UIView!
    @IBOutlet weak var lblHeaderChat: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        vwContentChat.clipsToBounds = true
        vwContentChat.layer.cornerRadius = 5
    }

    func setView(text: String) {
        lblHeaderChat.text = text
    }
}
