//
//  BubbleDocsSenderViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 30/01/22.
//

import Foundation
import UICircularProgressRing
import UIKit

protocol BubbleDocsCancelDelegate: AnyObject {
    func cancelUpload()
}

class BubbleDocsSenderViewCell: UITableViewCell {

    @IBOutlet weak var lblDateSend: UILabel!
    @IBOutlet weak var ivImageStatusChat: UIImageView!
    @IBOutlet weak var lblChatMessage: UILabel!
    @IBOutlet weak var ivDocsLogo: UIImageView!
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var ivDoneUpload: UIImageView!
    @IBOutlet weak var progressBar: UICircularProgressRing!
    
    var chatModel: MessageModel?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?
    var onClickOpenFile: (() -> Void)?
    var nameOfFile: String = ""
    private let notificationCenter = NotificationCenter.default
    weak var delegate: BubbleDocsCancelDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        vwContent.clipsToBounds = true
        vwContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell() {
        progressBar.isHidden = true
        ivDoneUpload.isHidden = false
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"

        if let data = chatModel {
            lblChatMessage.text = nameOfFile
            lblChatMessage.textColor = UIColor(hex: "FFFCFC")
            lblDateSend.text = dateFormatter.string(from: data.timestamp ?? Date())

            if data.status == "PENDING" {
                initNotifCenter()
                progressBar.isHidden = false
                ivDoneUpload.isHidden = true
                ivImageStatusChat.image = UIImage(named: "icon-pending", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "RECEIVED" {
                notificationCenter.removeObserver(self)
                ivImageStatusChat.image = UIImage(named: "icon-message-send", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "SENDING" {
                ivImageStatusChat.image = UIImage(named: "icon-message-delivered", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            } else if data.status == "READ" {
                ivImageStatusChat.image = UIImage(named: "icon-message-read", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
            }

            if let url = URL(string: data.documentSource ?? "") {
                let fileName = url.lastPathComponent
                switch fileName.getFileType() {
                case .doc:
                    ivDocsLogo.image = UIImage(named: "icon-doc", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .docx:
                    ivDocsLogo.image = UIImage(named: "icon-word", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .pdf:
                    ivDocsLogo.image = UIImage(named: "icon-pdf", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .xlsx:
                    ivDocsLogo.image = UIImage(named: "icon-excel", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .pptx:
                    ivDocsLogo.image = UIImage(named: "icon-pptx", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .audio:
                    ivDocsLogo.image = UIImage(named: "icon-audio", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                }
            }
        }
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    @IBAction func onclickCancel(_ sender: Any) {
        delegate?.cancelUpload()
    }
    
    func initNotifCenter() {
        notificationCenter.addObserver(self, selector: #selector(uploadStart), name: Notification.Name("upload-start"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(uploadProgress(notification:)), name: Notification.Name("upload-progress"), object: nil)
    }

    @IBAction func onClickOptionnsChat(_ sender: Any) {
        onClickChatOptions?(chatModel!)
    }

    @objc func uploadStart() {
        progressBar.value = 10
    }

    @objc func uploadProgress(notification: Notification) {
        if let data = notification.userInfo {
            if let progress = data["progress"] as? Double {
                progressBar.value = progress
                if progress == 1 {
                    ivImageStatusChat.image = UIImage(named: "icon-message-send", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                }
            }
        }
    }
    
    @IBAction func onClickOpenFile(_ sender: Any) {
        onClickOpenFile?()
    }
}
