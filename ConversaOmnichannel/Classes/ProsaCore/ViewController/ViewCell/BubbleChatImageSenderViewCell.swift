//
//  BubbleChatImageSenderViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 12/01/22.
//

import UIKit

class BubbleChatImageSenderViewCell: UITableViewCell {

    
    @IBOutlet weak var vwContentChat: UIView!
    @IBOutlet weak var ivImageChat: UIImageView!
    @IBOutlet weak var lblSendDate: UILabel!
    
    var onClick: ((_ data: MessageModel) -> Void)?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?
    var data: MessageModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        vwContentChat.clipsToBounds = true
        vwContentChat.layer.cornerRadius = 5
    }

    func setView() {
        if let data = data {
            ivImageChat.layer.cornerRadius = 5
            ivImageChat.loadImage(url: data.documentSource ?? "")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            lblSendDate.text = dateFormatter.string(from: data.timestamp ?? Date())
        }
    }
    
    @IBAction func onClick(_ sender: Any) {
        if let data = data {
            onClick?(data)
        }
    }

    @IBAction func onClickChatOptions(_ sender: Any) {
        onClickChatOptions?(data!)
    }
}
