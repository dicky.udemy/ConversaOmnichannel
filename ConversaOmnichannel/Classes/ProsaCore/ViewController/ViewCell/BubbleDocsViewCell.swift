//
//  BubbleDocsViewCell.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 06/04/22.
//

import Foundation
import UICircularProgressRing
import UIKit


class BubbleDocsViewCell: UITableViewCell {

    @IBOutlet weak var lblDateSend: UILabel!
    @IBOutlet weak var lblChatMessage: UILabel!
    @IBOutlet weak var ivDocsLogo: UIImageView!
    @IBOutlet weak var vwContent: UIView!
    
    var chatModel: MessageModel?
    var onClickChatOptions: ((_ data: MessageModel) -> Void)?
    var onClickOpenFile: (() -> Void)?
    var nameOfFile: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()

        vwContent.clipsToBounds = true
        vwContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"

        if let data = chatModel {
            lblChatMessage.text = nameOfFile
            lblChatMessage.textColor = UIColor(hex: "121212")
            lblDateSend.text = dateFormatter.string(from: data.timestamp ?? Date())

            if let url = URL(string: data.documentSource ?? "") {
                let fileName = url.lastPathComponent
                switch fileName.getFileType() {
                case .doc:
                    ivDocsLogo.image = UIImage(named: "icon-doc", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .docx:
                    ivDocsLogo.image = UIImage(named: "icon-word", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .pdf:
                    ivDocsLogo.image = UIImage(named: "icon-pdf", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .xlsx:
                    ivDocsLogo.image = UIImage(named: "icon-excel", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .pptx:
                    ivDocsLogo.image = UIImage(named: "icon-pptx", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                case .audio:
                    ivDocsLogo.image = UIImage(named: "icon-audio", in: Bundle(identifier: Constant.bundleId), compatibleWith: nil)
                }
            }
        }
    }

    @IBAction func onClickOptionnsChat(_ sender: Any) {
        onClickChatOptions?(chatModel!)
    }

    @IBAction func onClickOpenFile(_ sender: Any) {
        onClickOpenFile?()
    }
}
