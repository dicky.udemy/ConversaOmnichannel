//
//  PublicFunction.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 08/01/22.
//

import Foundation
import SwiftyJSON
import UIKit

public enum Environment: String {
    case Development = "Development"
    case Staging = "Staging"
    case Production = "Production"
}

public class ProsaOmninchannel {
    static
    let socketManager = SocketIoManager.sharedInstance

    public static var environment: Environment? {
       get {
           guard let environment = UserDefaults.standard.value(forKey: Constant.IS_DEVELOPMENT) as? String else {
               return nil
           }
           return Environment(rawValue: environment)
       }
       set(environment) {
           UserDefaults.standard.set(environment?.rawValue, forKey: Constant.IS_DEVELOPMENT)
       }
    }

    public static func openChat(avatarUrl: String, password: String) -> UIViewController {
        let vc = ChatRoomViewController()
        vc.avatarUrl = avatarUrl
        vc.password = password
        vc.viewModel?.registerFont()
        
        return vc
    }

    public static func setupChat(_ userName: String, _ userId: String, appId: String) {
        UserDefaults.standard.set(appId, forKey: Constant.APP_ID)
        UserDefaults.standard.set(userName, forKey: Constant.USER_NAME)
        UserDefaults.standard.set(userId, forKey: Constant.USER_ID)
        UserDefaults.standard.synchronize()
    }

    public static func resetData() {
        Dlog("Reset Enviromet")
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
        UserDefaults.standard.synchronize()
    }

    public static func stopConnection() {
        socketManager.stop()
    }
}
