//
//  Constant.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 03/01/22.
//

import Foundation

class Constant {
    static let bundleId: String = "Prosa.ProsaCore"

    // User default
    
    static let USER_TOKEN: String = "USER_TOKEN"
    static let USER_REFRESH_TOKEN: String = "USER_REFRESH_TOKEN"
    static let USER_ID: String = "USER_ID"
    static let NONCE: String = "NONCE"
    static let IS_DEVELOPMENT: String = "IS_DEVELOPMENT"
    static let APP_ID: String = "APP_ID"
    static let USER_NAME: String = "USER_NAME"
    static let DOMAIN: String = "https://conversa-dev-93wirujo.prosa.ai/api-socket/dev"
}

enum Filetype {
    case doc
    case docx
    case pdf
    case xlsx
    case pptx
    case audio
}
