//
//  BaseVC.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 09/01/22.
//

import UIKit
import RxSwift
import RxCocoa
import Toast

protocol ViewModelBased: class {
    associatedtype ViewModel
    var viewModel: ViewModel? { get set }
}

class BaseNetworkViewController<D: BaseViewModel>: BaseViewController<D> {
    func hideKeyboard() {
        self.view.endEditing(true)
    }
}

class BaseViewController<D: BaseViewModel>: GenericViewController<D> {

    var remoteConfigApplied = false

    override func registerObserver() {
        super.registerObserver()
    }
}


class BaseViewModel: NSObject {
    var message = BehaviorRelay<String?>(value: nil)
    var isLoading = BehaviorRelay<Bool>(value: false)
    
    required override init() {
        super.init()
    }
}

class GenericViewController<D> : UIViewController, ViewModelBased where D: BaseViewModel {
    var disposeBag = DisposeBag()
    var viewModel: D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        self.registerObserver()
    }
    
    private func setupViewModel() {
        if viewModel == nil {
            viewModel = D()
        }
    }
    
    func registerObserver() {
        viewModel?.message.subscribe(onNext: { [weak self] message in
            if let messages = message, self?.viewIfLoaded?.window != nil {
                self?.view.makeToast(messages)
            }
        }).disposed(by: disposeBag)
    }
}

class NetworkViewModel: BaseViewModel {
    
    var responseProceeded: Bool = false
    
    var isTimeout = BehaviorRelay<Bool>(value: false)
    var timeoutResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    
    var delegate: NetworkViewModelDelegate?
    
    func process(response: BehaviorRelay<NetworkResponse?>?, onSuccess: ((NetworkResponse) -> Void)?) {
        responseProceeded = true
        
        guard let response = response, let value = response.value else {
            return
        }
        
        if value.isSuccess() {
            onSuccess?(value)
        } else if value.isTimeoutError() {
            isTimeout.accept(true)
            isLoading.accept(false)
            timeoutResponse = response
        } else {
            isLoading.accept(false)
            self.message.accept(value.errorMessage)
        }
    }
    
    var gotResponseStatus: Bool {
        return responseProceeded
    }
}

protocol NetworkViewModelDelegate {
    func onRetry()
}
