//
//  DocumentPickerController.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 08/01/22.
//

import MobileCoreServices
import UIKit

protocol DocumetPickerDelegate: AnyObject {
    func didSelect(document: URL)
}

class DocumentPicker: NSObject {
    private let pickerController: UIDocumentPickerViewController
    private weak var presentationController: UIViewController?
    private weak var delegate: DocumetPickerDelegate?

    init(presentationController: UIViewController, delegate: DocumetPickerDelegate) {
        self.pickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate

        self.pickerController.delegate = self
    }

    func present() {
        self.presentationController?.present(self.pickerController, animated: true)
    }
}

extension DocumentPicker: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.delegate?.didSelect(document: myURL)
    }
}
