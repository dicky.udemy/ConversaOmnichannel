//
//  Array+Ext.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 05/01/22.
//

import Foundation
import UIKit

func removeHTMLTags(for regex: String!, in text: String!) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

extension Array where Element: Equatable {
    
    mutating func remove(element: Element) {
        if let index = firstIndex(of: element) {
            remove(at: index)
        }
    }
}

extension Decodable {
    init(from any: Any) throws {
        let data = try JSONSerialization.data(withJSONObject: any)
        self = try JSONDecoder().decode(Self.self, from: data)
    }
}

extension String {
    func isEqualLowercased(_ text: String) -> Bool {
        return self.lowercased().contains(text.lowercased())
    }

    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }

        return try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil
        )
    }

    func getFileType() -> Filetype {
        if self.isEqualLowercased("DOCX") {
            return .docx
        } else if self.isEqualLowercased("DOC") {
            return .doc
        } else if self.isEqualLowercased("PPTX") {
            return .pptx
        } else if self.isEqualLowercased("PDF") {
            return .pdf
        } else if self.isEqualLowercased("XLSX") {
            return .xlsx
        } else {
            return .audio
        }
    }

    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }

    func isImage() -> Bool {
        if self.isEqualLowercased("PNG") || self.isEqualLowercased("JPG") || self.isEqualLowercased("JPEG") {
            return true
        } else {
            return false
        }
    }
}

extension Date {
    func reduceToMonthDayYear() -> Date {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        let year = calendar.component(.year, from: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: "\(month)/\(day)/\(year)") ?? Date()
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

enum FontError: Error {
    case invalidFontFile
    case fontPathNotFound
    case initFontError
    case registerFailed
    case bundleNotFound
}

extension UIFont {
    static func register(fileNameString: String, type: String) {
        guard let frameworkBundle = Bundle(identifier: Constant.bundleId) else {
            print(FontError.bundleNotFound)
            return
        }
        guard let resourceBundleURL = frameworkBundle.path(forResource: fileNameString, ofType: type) else {
            print(FontError.fontPathNotFound)
            return
        }
        guard let fontData = NSData(contentsOfFile: resourceBundleURL), let dataProvider = CGDataProvider.init(data: fontData) else {
            print(FontError.invalidFontFile)
            return
        }
        guard let fontRef = CGFont.init(dataProvider) else {
            print(FontError.initFontError)
            return
        }
        var errorRef: Unmanaged<CFError>? = nil
        guard CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) else   {
            print(FontError.registerFailed)
            return
        }
    }
}

func Dlog(_ message: String,
          function: String = #function,
          file: String = #file,
          line: Int = #line,
          column: Int = #column) {
    #if DEBUG
    let string = "\n====================================================\nfile: \(file)\nfunction: \(function)\nline: \(line)\ncolumn: \(column)\nMESSAGE: \(message)\n\n\n"
    print("\(string)")
    #else
    #endif
}

extension URL {
    var hasHiddenExtension: Bool {
        get { (try? resourceValues(forKeys: [.hasHiddenExtensionKey]))?.hasHiddenExtension == true }
        set {
            var resourceValues = URLResourceValues()
            resourceValues.hasHiddenExtension = newValue
            try? setResourceValues(resourceValues)
        }
    }
}
