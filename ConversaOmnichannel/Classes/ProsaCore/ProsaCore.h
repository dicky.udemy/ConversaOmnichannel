//
//  ProsaCore.h
//  ProsaCore
//
//  Created by Dicky Geraldi on 02/01/22.
//

#import <Foundation/Foundation.h>

//! Project version number for ProsaCore.
FOUNDATION_EXPORT double ProsaCoreVersionNumber;

//! Project version string for ProsaCore.
FOUNDATION_EXPORT const unsigned char ProsaCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProsaCore/PublicHeader.h>

