//
//  RemoteDataSource.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 09/01/22.
//

import RxCocoa

class RemoteDataResource {
    let baseApi: BaseApi = BaseApi()
}

class ChatRoomViewDataSource: RemoteDataResource {

    func cancelUpload() {
        baseApi.uploadTask.cancel()
    }

    func getUserProfile(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.postMethod(partUrl: ListUrls.LOGIN, params: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    func postCheckToken(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.postMethod(partUrl: ListUrls.CHECK_TOKEN, params: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    func postRefreshToken(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.postMethod(partUrl: ListUrls.REFRESH_TOKEN, params: params, isNeedRefres: true) { (response) in
            networkResponse.accept(response)
        }
    }

    func uploadImage(file: URL, fileData: Data, networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.uploadFile(partUrl: ListUrls.UPLOAD_FILE, data: file, imageFile: fileData) { (response) in
            networkResponse.accept(response)
        }
    }

    func deleteMessage(serverChatId: String, networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.deleteMethod(partUrl: "\(ListUrls.DELETE_CHAT)\(serverChatId)") { (response) in
            networkResponse.accept(response)
        }
    }

    func getDetailBot(networkResponse: BehaviorRelay<NetworkResponse?>) {
        baseApi.get(with: ListUrls.PROFILE_CHATBOT, completion: { (response) in
            networkResponse.accept(response)
        })
    }
}
