//
//  ListUrls.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 07/01/22.
//

struct ListUrls {
    // WEBSOCKEt
    static let WEBSOCKET_URL_DEVELOPMENT = "https://conversa-dev-93wirujo.prosa.ai/"
    static let WEBSOCKET_URL_STAGING = "https://conversa-stg.prosa.ai/"
    static let WEBSOCKET_URL_PRODUCTION = ""

    // API List
    static let LOGIN = "/chat-inapp-omnichannel"
    static let GET_NONCE = "/nonce"
    static let GET_NONCE_DUMMY = "/dummy-token"
    static let CHECK_TOKEN = "/token/check"
    static let REFRESH_TOKEN = "/token/refresh"
    static let UPLOAD_FILE = "/upload-file"
    static let DELETE_CHAT = "/delete-chat?chat_id="
    static let PROFILE_CHATBOT = "/chatbot-profile-short"
}
