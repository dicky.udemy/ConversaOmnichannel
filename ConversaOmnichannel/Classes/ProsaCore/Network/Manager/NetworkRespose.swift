//
//  NetworkRespose.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 07/01/22.
//

import SwiftyJSON

class NetworkResponse {
    static let RETRYABLE_ERROR = -1000
    
    var request: URLRequest?
    var response: Any?
    var reqCode: Int = 0
    var statusCode: Int?
    var errorMessage: String?

    init(requestCode reqCode: Int = 0) {
        self.reqCode = reqCode
    }

    init(requestCode reqCode: Int = 0, withStatusCode statusCode: Int) {
        self.reqCode = reqCode
        self.statusCode = statusCode
        
        if isServerError() && !isMaintenanceError() && !isTooManyRequestError() {
            self.errorMessage = "Gagal Menghubungi Server"
        }
    }
    
    init(requestCode reqCode: Int = 0, withResponse response: Any) {
        self.response = response
        self.reqCode = reqCode
    }
    
    func isSuccess() -> Bool {
        return errorMessage?.isEmpty ?? true && response != nil && !isServerError() && !isLocalDeviceError()
    }
    
    func isServerError() -> Bool {
        return statusCode != nil && ((statusCode! >= 500 && statusCode! <= 503) || (statusCode! >= 400 && statusCode! <= 419) || isTooManyRequestError())
    }
    
    func isLocalDeviceError() -> Bool {
        return isDeviceTokenExpired() || isUserTokenExpired() || isTimeMissMatch()
    }
    
    func isHostNotFound() -> Bool {
        return statusCode == URLError.Code.cannotFindHost.rawValue
    }
    
    func isTimeoutError() -> Bool {
        return statusCode == URLError.Code.timedOut.rawValue
    }
    
    func isMaintenanceError() -> Bool {
        return statusCode == 503
    }
    
    func isTooManyRequestError() -> Bool {
        return statusCode == 429
    }
    
    func isRetryableError() -> Bool {
        return statusCode == NetworkResponse.RETRYABLE_ERROR
    }
    
    func isDeviceTokenExpired() -> Bool {
        return isErrorOnDevice(with: 330)
    }
    
    func isUserTokenExpired() -> Bool {
        return isErrorOnDevice(with: 333)
    }
    
    func isTimeMissMatch() -> Bool {
        return isErrorOnDevice(with: 444)
    }
    
    func isErrorOnDevice(with errorCode: Int) -> Bool {
        guard let response = self.response else {
            return false
        }
        
        let json = JSON(response)
        
        guard !json["status"].boolValue else {
            return false
        }
        
        return json["meta"]["code"].intValue == errorCode
    }
}
