//
//  Constant.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 08/01/22.
//

import Foundation

enum SenderType {
    case User
    case Bot
    case CS
}
