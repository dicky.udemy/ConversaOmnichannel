//
//  ApiManager.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 07/01/22.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import CoreServices

class BaseApi: NSObject {
    let env: Environment = ProsaOmninchannel.environment ?? .Development
    var urlSession = URLSession.shared
    var uploadTask = URLSessionDataTask()
    
    let alamofire: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
    
    func getUrl(partUrl: String) -> String {
        var baseUrl: String = ""
        if env == .Development {
            baseUrl = "https://conversa-dev-93wirujo.prosa.ai/api/dev"
        } else if env == .Staging {
            baseUrl = "https://conversa-stg.prosa.ai/api/bluebird-prosa"
        } else {
            // next release
        }
        
        return baseUrl + partUrl
    }
    
    func get(with partUrl: String, params: [String: String] = [:], completion: @escaping (NetworkResponse) -> Void) {
        let fullUrl: String = getUrl(partUrl: partUrl)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Dlog("Request to: \(fullUrl)")
        
        alamofire.request(fullUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: generateHeader(isUploadImage: false)).responseJSON {
            (dataResponse) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let response = dataResponse
            let httpStatusCode = response.response?.statusCode
            switch(response.result) {
            case .success(let JSON):
                completion(self.generateNetworkResponse(request: response.request, result: .success(response.result.isSuccess), response: JSON, httpStatusCode: httpStatusCode))
                
            case .failure(let error):
                completion(self.generateNetworkResponse(request: response.request, result: .failure(error), error: error, httpStatusCode: httpStatusCode))
            }
        }
    }
    
    func postMethod(partUrl: String, params: [String: Any], isNeedRefres: Bool = false, completion: @escaping (NetworkResponse) -> Void) {
        let fullUrl: String = getUrl(partUrl: partUrl)

        guard let url = URL(string: fullUrl) else {
            Dlog("Error: cannot create URL")
            return
        }

        guard let postData = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }

        Dlog("Request to: \(fullUrl)")
        Dlog("Request Body \(params)")

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if !isNeedRefres {
            if let accessToken = UserDefaults.standard.string(forKey: Constant.USER_TOKEN) {
                request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            }
        } else {
            if let accessToken = UserDefaults.standard.string(forKey: Constant.USER_REFRESH_TOKEN) {
                request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            }
        }
        request.httpBody = postData

        uploadTask = urlSession.dataTask(with: request) { data, response, error in
            guard error == nil else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }
                return
            }
            guard let data = data else {
                Dlog("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }

                return
            }
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    Dlog("Error: Cannot convert data to JSON object")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    Dlog("Error: Cannot convert JSON object to Pretty JSON data")
                    return
                }

                completion(self.generateNetworkResponse(request: request, result: .success(true), response: JSON(prettyJsonData), httpStatusCode: response.statusCode))
            } catch {
                Dlog("Error: Trying to convert JSON data to string")

                return
            }
        }
        uploadTask.resume()
    }
    
    private func mimeType(for path: String) -> String {
        let pathExtension = URL(fileURLWithPath: path).pathExtension as NSString
        guard
            let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension, nil)?.takeRetainedValue(),
            let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue()
        else {
            return "application/octet-stream"
        }
 
        return mimetype as String
    }

    func uploadFile(partUrl: String, data: URL, imageFile: Data, completion: @escaping (NetworkResponse) -> Void) {
        let fullUrl: String = getUrl(partUrl: partUrl)
        guard let url = URL(string: fullUrl) else {
            Dlog("Error: cannot create URL")
            return
        }

        let boundary = UUID().uuidString
        var request = URLRequest(url: url)
        request.cachePolicy = .useProtocolCachePolicy
        request.httpMethod = "POST"
        if let accessToken = UserDefaults.standard.string(forKey: Constant.USER_TOKEN) {
            request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        }
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        var fileName = data.lastPathComponent
        let mimetype = mimeType(for: fileName)
        let paramName = "file"
        var data = Data()

        if fileName.isEmpty {
            let date = Date()
            let dateFormatter = DateFormatter()

            dateFormatter.dateFormat = "hh:mm:ss"
            fileName = "\(dateFormatter.string(from: date)).png"
        }

        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
                data.append(imageFile)
                data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        request.setValue(String(data.count), forHTTPHeaderField: "Content-Length")
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: .main)
        
        session.uploadTask(with: request, from: data) { data, response, error in
            guard error == nil else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }
                return
            }
            guard let data = data else {
                Dlog("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }

                return
            }
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    Dlog("Error: Cannot convert data to JSON object")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    Dlog("Error: Cannot convert JSON object to Pretty JSON data")
                    return
                }

                completion(self.generateNetworkResponse(request: request, result: .success(true), response: JSON(prettyJsonData), httpStatusCode: response.statusCode))
            } catch {
                Dlog("Error: Trying to convert JSON data to string")

                return
            }
        }.resume()
    }

    func generateHeader(isUploadImage: Bool) -> [String: String] {
        var header: [String: String] = [:]
        
        if isUploadImage {
            header["Content-Type"] = "multipart/form-data"
        } else {
            header["Content-Type"] = "application/json"
        }
        
        return header
    }
    
    func generateNetworkResponse(request: URLRequest?, result: Result<Bool>, response: Any? = nil, error: Any? = nil, httpStatusCode: Int? = nil) -> NetworkResponse {
        
        var networkResponse = NetworkResponse.init()
        
        if let statusCode = httpStatusCode {
            networkResponse = NetworkResponse.init(withStatusCode: statusCode)
        }
        
        if result.isSuccess && response != nil {
            let json = JSON(response!)

            networkResponse.response = json
        } else if !networkResponse.isServerError() {
            let json = JSON(response!)

            networkResponse.response = json
            if let urlError = error as? URLError, urlError.code == .cancelled {
                return networkResponse
            }
            if let urlError = error as? URLError,
               urlError.code == .cannotFindHost || urlError.code == .timedOut {
                networkResponse.statusCode = urlError.code.rawValue
            } else {
                networkResponse.statusCode = NetworkResponse.RETRYABLE_ERROR
            }
        }
        
        networkResponse.request = request
        Dlog("Final network response \(networkResponse.response ?? "")")
        Dlog("Final network httpStatusCode \(httpStatusCode ?? 0)")
        
        return networkResponse
    }

    func deleteMethod(partUrl: String, completion: @escaping (NetworkResponse) -> Void) {
        let fullUrl: String = getUrl(partUrl: partUrl)
        guard let url = URL(string: fullUrl) else {
            Dlog("Error: cannot create URL")
            return
        }

        Dlog("Request to: \(fullUrl)")

        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if let accessToken = UserDefaults.standard.string(forKey: Constant.USER_TOKEN) {
            request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        }

        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }
                return
            }
            guard let data = data else {
                Dlog("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                if let response = response as? HTTPURLResponse {
                    completion(self.generateNetworkResponse(request: request, result: .success(false), error: error, httpStatusCode: response.statusCode))
                }

                return
            }
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    Dlog("Error: Cannot convert data to JSON object")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    Dlog("Error: Cannot convert JSON object to Pretty JSON data")
                    return
                }

                completion(self.generateNetworkResponse(request: request, result: .success(true), response: JSON(prettyJsonData), httpStatusCode: response.statusCode))
            } catch {
                Dlog("Error: Trying to convert JSON data to string")

                return
            }
        }.resume()
    }
}

extension BaseApi: URLSessionTaskDelegate {
    func urlSession(
            _ session: URLSession,
            task: URLSessionTask,
            didSendBodyData bytesSent: Int64,
            totalBytesSent: Int64,
            totalBytesExpectedToSend: Int64
        ) {
            let uploadProgress: Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: Notification.Name("upload-progress"), object: ["progress": uploadProgress])
            })
        }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        Dlog("urlSession -> didCompleteWithError")
    }
}
