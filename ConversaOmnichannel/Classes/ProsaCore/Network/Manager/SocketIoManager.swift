//
//  SocketIoManager.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 13/01/22.
//

import SocketIO
import Foundation
import SwiftyJSON

protocol SocketIoDelegate {
    func messageReceived(message: MessageModel)
    func messageReadByServer(clientId: Int)
    func messageReceivedServer(clientId: Int)
    func messageSend(message: MessageModel)
    func messageHistoryReceived(message: [MessageModel])
    func messageServerIdReceived(serverId: String, clientId: Int)
    func messageAgentJoined(message: MessageModel)
    func statusSocket(isConneected: Bool, isReconnect: Bool)
}

class SocketIoManager: NSObject {
    // MARK: - Properties
    static let sharedInstance = SocketIoManager()
    let env: Environment = ProsaOmninchannel.environment ?? .Development
    var manager = SocketManager(socketURL: URL(string: ListUrls.WEBSOCKET_URL_DEVELOPMENT)!, config: [.compress])
    var socket: SocketIOClient?
    var room: Int = 0
    var delegate: SocketIoDelegate?

    func stop() {
        socket?.removeAllHandlers()
        socket?.disconnect()
    }

    override init() {
        super.init()
        self.manager = SocketManager(socketURL: self.getUrl(), config: [.compress])
    }
    
    func getUrl() -> URL {
        var socketUrl: String = ""
        if env == .Development {
            socketUrl = ListUrls.WEBSOCKET_URL_DEVELOPMENT
        } else if env == .Staging {
            socketUrl = ListUrls.WEBSOCKET_URL_STAGING
        } else {
            // next release
        }
        
        return URL(string: socketUrl)!
    }
    
    func getSocketPath() -> String {
        var socketPath: String = ""
        if env == .Development {
            socketPath = "/api-socket/dev/socket.io"
        } else if env == .Staging {
            socketPath = "/api-socket/bluebird-prosa/socket.io"
        } else {
            // next release
        }
        
        return socketPath
    }
    
    // MARK: - Socket Setup
    func setupSocket() {
        if let token = UserDefaults.standard.string(forKey: Constant.USER_TOKEN) {
//            let manager = SocketManager(socketURL: getUrl(), config: [.compress])
            manager.config = SocketIOClientConfiguration(arrayLiteral:
                .extraHeaders(["Authorization": "Bearer \(token)"]),
                .log(true),
                .path(getSocketPath()),
                .secure(true),
                .reconnects(true),
                .reconnectWait(10)
                )
            socket = manager.defaultSocket

            setupSocketEvents()
            socket?.connect()
        }
    }

    func setupSocketEvents() {
        socket?.on(clientEvent: .disconnect) { data, ack in
            self.delegate?.statusSocket(isConneected: false, isReconnect: false)
        }

        socket?.on(clientEvent: .reconnect) { data, ack in
            self.delegate?.statusSocket(isConneected: false, isReconnect: true)
        }
    
        socket?.on("connect") { data, ack in
            self.delegate?.statusSocket(isConneected: true, isReconnect: false)
        }
        
        socket?.on("session_id") { [weak self] data, ack in
            guard let roomId = data.first as? Int else {
                return
            }

            self?.room = roomId
        }

        socket?.on("history") { (data, ack) in
            guard let historyChat = data.first else { return }
            var message: [MessageModel] = []

            let json = JSON(historyChat).arrayValue
            for data in json {
                message.append(MessageModel(data))
            }

            self.delegate?.messageHistoryReceived(message: message)
        }

        socket?.on("chat") { (data, ack) in
            guard let getMessage = data.first else { return }
            let json = JSON(getMessage)

            self.delegate?.messageReceived(message: ParseMessageModel(json, status: "SENDING"))
        }

        socket?.on("read") { (data, ack) in
            guard let getMessage = data.first else { return }
            let json = JSON(getMessage)

            self.delegate?.messageReadByServer(clientId: json["client_chat_id"].intValue)
        }

        socket?.on("server-chat-id") { (data, ack) in
            guard let dataInfo = data.first else { return }
            let json = JSON(dataInfo)

            self.delegate?.messageServerIdReceived(serverId: json["server_chat_id"].stringValue, clientId: json["client_chat_id"].intValue)
        }
        
        socket?.on("handled") { data, ack in
            guard let dataInfo = data.first else { return }
            let json = JSON(dataInfo)

            self.delegate?.messageAgentJoined(message: ParseMessageHandled(json, isResolved: false))
        }

        socket?.on("done-session") { data, ack in
            guard let dataInfo = data.first else { return }
            let json = JSON(dataInfo)

            self.delegate?.messageAgentJoined(message: ParseMessageHandled(json, isResolved: true))
        }
    }

    // MARK: - Socket Emits

    func send(message: String, replyTo: String, source: String, documentUrl: String, clientChatId: Int, docName: String = "") {
        var params: [String: Any] = [:]
        if message.isEmpty {
            if documentUrl.isImage() {
                params["text"] = chatImageMessage(url: documentUrl, docName: docName)
            } else {
                params["text"] = chatDocMessage(url: documentUrl, docName: docName)
            }
        } else {
            params["text"] = message
        }
        params["room"] = room
        params["client_chat_id"] = clientChatId
        params["source"] = source
        params["reply_to"] = replyTo
        params["document_source"] = documentUrl
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
        if documentUrl.isEmpty {
            self.delegate?.messageSend(message: ParseMessageModelFromParams(params, status: "PENDING"))
        }
        
        socket?.emitWithAck("chat", jsonString).timingOut(after: 20) { data in
            self.delegate?.messageReceivedServer(clientId: params["client_chat_id"] as! Int)
        }
    }
    
    func chatImageMessage(url: String, docName: String) -> String {
        return """
        <img src="\(url)"   class="cursor-pointer" style="max-width: 200px;"/>
            <div class="row items-center display-flex">
            <div class="bb-paragraph q-ml-sm row items-end display-block">
              <div class="col-12 text-white" style="max-width: 160px">\(docName)</div>
           </div>
        </div>
        """
    }
    
    func chatDocMessage(url: String, docName: String) -> String {
        return """
        <div class="row items-center">
           <img src=statics/icons/icon_pdf.png class="q-mr-sm" style="max-width: 50px">
           <span url="\(url)" class="is-document">\(docName)</span>
        </div>
        """
    }
    
    func read(serverChatId: String) {
        var params: [String: Any] = [:]
        params["server_chat_id"] = serverChatId
        params["room"] = room
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!

        socket?.emit("read", jsonString)
    }
}
