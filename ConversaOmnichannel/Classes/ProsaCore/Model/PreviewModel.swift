//
//  PreviewModel.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 31/01/22.
//

import QuickLook

class Preview: NSObject, QLPreviewItem {
    var previewItemURL: URL?
    var url: URL
    var urlPrefix: String
    
    init(url: URL, urlPrefix: String) {
        self.url = url
        self.urlPrefix = urlPrefix

        super.init()
    }
}
