//
//  MessageModel.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 16/01/22.
//

import Foundation
import SwiftyJSON

class MessageModel {
    var info: MessageInfo?
    var isDeleted: Bool?
    var name: String?
    var serverChatId: String?
    var clientChatId: Int?
    var source: String?
    var status: String?
    var text: String?
    var timestamp: Date?
    var replyTo: String?
    var documentSource: String?

    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }

        self.info = MessageInfo.init(JSON(json["info"]))
        self.isDeleted = json["is_deleted"].boolValue
        self.name = json["name"].stringValue
        self.serverChatId = json["server_chat_id"].stringValue
        self.source = json["source"].stringValue
        self.status = json["status"].stringValue
        self.text = json["text"].stringValue
        self.clientChatId = json["client_chat_id"].intValue
        self.replyTo = json["reply_to"].stringValue
        self.documentSource = json["document_source"].stringValue
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MMMM dd, yyyy HH:mm:ss"
        self.timestamp = dateFormatterGet.date(from: json["timestamp"].stringValue) ?? Date()
    }

    func isSender() -> Bool {
        if self.source ?? "" == "USER" {
            return true
        } else {
            return false
        }
    }
    
    func isHtmlDoc() -> Bool {
        if let text = self.text {
            if text.contains("<img") {
                return true
            } else if text.contains("<span url") {
                return true
            }
        }
        return false
    }
    
    func getHtmltag() -> String {
        if let text = self.text {
            var strNew = text.components(separatedBy: .newlines).joined(separator: "")
            strNew = strNew.replacingOccurrences(of: "\"", with: "'")
            let url = removeHTMLTags(for: "(http[^\\s]+(jpg|JPG|jpeg|JPEG|png|PNG|heic|HEIC)\\b)", in: String(strNew))
            if let urlImage = url.first {
                return urlImage
            } else {
                let urlDoc = removeHTMLTags(for: "(http[^\\s]+(docx|DOCX|doc|DOC|pptx|PPTX|pdf|PDF|xlsx|XLSX)\\b)", in: String(strNew))
                if let urlDocument = urlDoc.first {
                    return urlDocument
                }
            }
        }
        
        return ""
    }
}

func ParseMessageModel(_ json: JSON? = "", status: String) -> MessageModel {
    let new = MessageModel()
    guard let json = json else {
        return new
    }

    new.info = MessageInfo.init(JSON(json["info"]))
    new.isDeleted = json["is_deleted"].boolValue
    new.name = json["name"].stringValue
    new.serverChatId = json["server_chat_id"].stringValue
    new.source = json["source"].stringValue
    new.status = status
    new.text = json["text"].stringValue
    new.clientChatId = json["client_chat_id"].intValue
    new.replyTo = json["reply_to"].stringValue
    new.documentSource = json["document_source"].stringValue
    
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "MMMM dd, yyyy HH:mm:ss"
    new.timestamp = dateFormatterGet.date(from: json["timestamp"].stringValue) ?? Date()

    return new
}

func ParseMessageHandled(_ json: JSON? = "", isResolved: Bool) -> MessageModel {
    let new = MessageModel()
    guard let json = json else {
        return new
    }

    new.timestamp = Date()
    new.clientChatId = nil
    if !isResolved {
        new.text = "\(json["type"].stringValue) \(json["name"].stringValue) is entering the chat"
    } else {
        new.text = "\(json["name"].stringValue) has resolved this conversation"
    }

    return new
}

func ParseMessageDate(_ params: [String: Any]) -> MessageModel {
    let new = MessageModel()

    new.timestamp = params["date"] as? Date
    new.text = nil
    new.clientChatId = nil

    return new
}

func ParseMessageModelFromParams(_ params: [String: Any], status: String) -> MessageModel {
    let new = MessageModel()

    new.source = params["source"] as? String
    new.status = status
    new.text = params["text"] as? String
    new.clientChatId = params["client_chat_id"] as? Int
    new.replyTo = params["reply_to"] as? String
    new.timestamp = Date()
    new.documentSource = params["document_source"] as? String

    return new
}

class MessageInfo {
    var confidenceScore: Bool?
    var isConfident: Bool?
    var responseType: String?
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }

        self.confidenceScore = json["confidence_score"].boolValue
        self.isConfident = json["is_confident"].boolValue
        self.responseType = json["response_type"].stringValue
    }
}
