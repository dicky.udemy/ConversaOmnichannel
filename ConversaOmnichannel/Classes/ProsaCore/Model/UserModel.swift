//
//  UserModel.swift
//  ProsaCore
//
//  Created by Dicky Geraldi on 09/01/22.
//

import SwiftyJSON

class UserModel {
    var accessToken: String?
    var refreshToken: String?
    var knowledgeGroups: [KnowledgeGroup] = []
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }
        
        accessToken = json["access_token"].stringValue
        refreshToken = json["refresh_token"].stringValue
        
        for object in json["knowledge_groups"].arrayValue {
            knowledgeGroups.append(KnowledgeGroup(object))
        }
    }
}

class KnowledgeGroup {
    var id: Int?
    var name: String?
    var description: String?

    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }

        id = json["id"].intValue
        name = json["name"].stringValue
        description = json["description"].stringValue
    }
}

class ProfileBotModel {
    var propicUrl: String = ""
    var chatbotName: String = ""
    var description: String = ""

    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }

        propicUrl = json["propic_url"].stringValue
        chatbotName = json["chatbot_name"].stringValue
        description = json["description"].stringValue
    }
}
