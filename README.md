# ConversaOmnichannel

[![CI Status](https://img.shields.io/travis/udemy-freelance/ConversaOmnichannel.svg?style=flat)](https://travis-ci.org/udemy-freelance/ConversaOmnichannel)
[![Version](https://img.shields.io/cocoapods/v/ConversaOmnichannel.svg?style=flat)](https://cocoapods.org/pods/ConversaOmnichannel)
[![License](https://img.shields.io/cocoapods/l/ConversaOmnichannel.svg?style=flat)](https://cocoapods.org/pods/ConversaOmnichannel)
[![Platform](https://img.shields.io/cocoapods/p/ConversaOmnichannel.svg?style=flat)](https://cocoapods.org/pods/ConversaOmnichannel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ConversaOmnichannel is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ConversaOmnichannel'
```

## Author

udemy-freelance, dicky.geraldi@cashbac.com

## License

ConversaOmnichannel is available under the MIT license. See the LICENSE file for more info.
